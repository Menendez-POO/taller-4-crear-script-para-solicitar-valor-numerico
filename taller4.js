/*Funcion para  el ingreso de un numero*/
function mostrar() {
    var numOctal = prompt("Ingrese un numero");

    /* Condiciones y comparacion de numero */
    if (numOctal == "") {
        alert("Proceda a ingresar un numero");
    } else {
        var numero = parseInt(numOctal, 8);

        alert("Numero decimal: " + parseInt(numero, 10));
    }
}

//Crear objeto
function Producto_alimenticio(codigo, nombre, precio) {
    this.codigo = codigo;
    this.nombre = nombre;
    this.precio = precio;
}
// Guardar en array productos
var productoArray = new Array();
productoArray[0] = new Producto_alimenticio(1, "Hamburguesas", 1.25);
productoArray[1] = new Producto_alimenticio(2, "Hot Dog", 1.00);
productoArray[2] = new Producto_alimenticio(3, "Jugos Naturales", 0.50);

// Funcion para imprimir datos
function imprimirDatos() {
    productoArray.forEach(producto => {
        document.write(`<h1 style="text-align:center;background-color:#333;color:white;font-size:25px;margin-top:40px">Registro ${productoArray.indexOf(producto)}<h1>`);
        document.write(`<p style="text-align:center;font-size:20px"><strong>Codigo:</strong> ${producto.codigo}</p>`);
        document.write(`<p style="text-align:center; font-size:20px"><strong>Nombre:</strong> ${producto.nombre}</p>`);
        document.write(`<p style="text-align:center;font-size:20px"><strong>Precio:</strong> ${producto.precio}</p>`);
    });
}
